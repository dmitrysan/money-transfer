package sankv.hw.money.transfer.repository.model;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Model that defines Account entity.
 */
public class Account {

    /**
     * Account id.
     */
    private String id;

    /**
     * Account balance.
     */
    private BigDecimal balance;

    /**
     * Constructs account object with UUID id and zero balance.
     */
    public Account() {
        this.id = UUID.randomUUID().toString();
        this.balance = new BigDecimal(0);
    }

    /**
     * Constructs account object.
     *
     * @param id account id
     * @param initialBalance account balance
     */
    public Account(String id, BigDecimal initialBalance) {
        this.id = id;
        this.balance = initialBalance;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Adds defined amount of money to account.
     *
     * @param amount amount of money to add to account
     */
    public void replenish(BigDecimal amount) {
        balance = balance.add(amount);
    }

    /**
     * Subtracts defined amount of money from account.
     *
     * @param amount amount of money to subtract from account
     */
    public void withdraw(BigDecimal amount) {
        balance = balance.subtract(amount);
    }

    /**
     * Transfer money from this account to another
     *
     * @param dest account to transfer money to
     * @param amount amount of money to transfer
     */
    public void transfer(Account dest, BigDecimal amount) {
        if(balance.compareTo(amount) < 0) {
            throw new IllegalArgumentException("Not enough money on balance");
        }
        this.withdraw(amount);
        dest.replenish(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(id, account.id)
                .append(balance, account.balance)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(id)
                .append(balance)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", balance=" + balance +
                '}';
    }
}
