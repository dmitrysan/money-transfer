package sankv.hw.money.transfer.repository;

import sankv.hw.money.transfer.repository.model.Account;

/**
 * Interface that defines account repository api.
 */
public interface AccountRepository {

    /**
     * Get account by id.
     *
     * @param id account id
     * @return account instance
     */
    Account getById(String id);

    /**
     * Save account information.
     *
     * @param account account to save
     * @return true if account successfully saved
     *         false if account was not saved
     */
    boolean save(Account account);
}
