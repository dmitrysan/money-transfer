package sankv.hw.money.transfer.repository;

import sankv.hw.money.transfer.repository.model.Account;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryAccountRepository implements AccountRepository {

    private Map<String, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public Account getById(String id) {
        return accounts.get(id);
    }

    @Override
    public boolean save(Account account) {
        accounts.put(account.getId(), account);
        return true;
    }

}
