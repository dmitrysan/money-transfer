package sankv.hw.money.transfer.service;

import sankv.hw.money.transfer.repository.AccountRepository;
import sankv.hw.money.transfer.repository.model.Account;

import java.math.BigDecimal;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getById(String id) {
        return accountRepository.getById(id);
    }

    @Override
    public boolean save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void transfer(String sourceAccountId, String destAccountId, BigDecimal amount) {
        final Account sourceAccount = accountRepository.getById(sourceAccountId);
        final Account destAccount = accountRepository.getById(destAccountId);

        int sourceAccountHash = System.identityHashCode(sourceAccount);
        int destAccountHash = System.identityHashCode(destAccount);

        if(sourceAccountHash <= destAccountHash) {
            synchronized (destAccount) {
                synchronized (sourceAccount) {
                    sourceAccount.transfer(destAccount, amount);
                }
            }
        } else if(sourceAccountHash > destAccountHash){
            synchronized (sourceAccount) {
                synchronized (destAccount) {
                    sourceAccount.transfer(destAccount, amount);
                }
            }
        }
    }
}
