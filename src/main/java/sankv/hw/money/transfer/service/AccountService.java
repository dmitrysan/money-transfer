package sankv.hw.money.transfer.service;

import sankv.hw.money.transfer.repository.model.Account;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * Get account by id.
     *
     * @param id account id
     * @return account instance
     */
    Account getById(String id);

    /**
     * Save account information.
     *
     * @param account account to save
     * @return true if account successfully saved
     *         false if account was not saved
     */
    boolean save(Account account);

    /**
     * Transfer money from source account to destination account.
     *
     * @param sourceAccountId account to transfer money to
     * @param destAccountId account to transfer money to
     * @param amount amount of money to transfer
     */
    void transfer(String sourceAccountId, String destAccountId, BigDecimal amount);
}
