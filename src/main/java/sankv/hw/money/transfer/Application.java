package sankv.hw.money.transfer;

import sankv.hw.money.transfer.config.ApplicationContext;
import sankv.hw.money.transfer.config.ConfigurableApplicationContext;
import sankv.hw.money.transfer.controller.AccountController;

import static spark.Spark.port;

public class Application {

    public static void main(String[] args) {
        port(8080);

        ConfigurableApplicationContext applicationContext = new ApplicationContext();

        AccountController accountController = applicationContext.getAccountController();

        accountController.initRoutes();
    }
}
