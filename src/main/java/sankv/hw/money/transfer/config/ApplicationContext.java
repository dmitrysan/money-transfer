package sankv.hw.money.transfer.config;

import sankv.hw.money.transfer.controller.AccountController;
import sankv.hw.money.transfer.repository.AccountRepository;
import sankv.hw.money.transfer.repository.InMemoryAccountRepository;
import sankv.hw.money.transfer.service.AccountService;
import sankv.hw.money.transfer.service.AccountServiceImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Creates instances of infrastructure classes.
 */
public class ApplicationContext implements ConfigurableApplicationContext {

    private Map<String, Object> beanCache = new HashMap<>();

    @Override
    public AccountRepository getAccountRepository() {
        return getFromCache(InMemoryAccountRepository.class, InMemoryAccountRepository::new);
    }

    @Override
    public AccountService getAccountService() {
        return getFromCache(AccountServiceImpl.class, () -> new AccountServiceImpl(getAccountRepository()));
    }

    @Override
    public AccountController getAccountController() {
        return getFromCache(AccountController.class, () -> new AccountController(getAccountService()));
    }

    @SuppressWarnings("unchecked")
    private <T> T getFromCache(Class<T> obj, Supplier<T> beanCreator) {
        T bean = (T) beanCache.get(obj.getCanonicalName());
        if (Objects.isNull(bean)) {
            bean = beanCreator.get();
            beanCache.put(obj.getCanonicalName(), bean);
        }
        return bean;
    }

}
