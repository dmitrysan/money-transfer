package sankv.hw.money.transfer.config;

import sankv.hw.money.transfer.controller.AccountController;
import sankv.hw.money.transfer.repository.AccountRepository;
import sankv.hw.money.transfer.service.AccountService;

/**
 * Interface that defines application dependencies.
 */
public interface ConfigurableApplicationContext {

    AccountRepository getAccountRepository();

    AccountService getAccountService();

    AccountController getAccountController();

}
