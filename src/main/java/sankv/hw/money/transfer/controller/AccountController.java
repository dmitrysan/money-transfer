package sankv.hw.money.transfer.controller;

import com.google.gson.Gson;
import sankv.hw.money.transfer.controller.model.TransferModel;
import sankv.hw.money.transfer.repository.model.Account;
import sankv.hw.money.transfer.service.AccountService;
import spark.Spark;

import java.math.BigDecimal;

/**
 * Account controller defines REST API for Account entity.
 */
public class AccountController {

    private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    private AccountService accountService;

    private Gson gson;

    /**
     * Constructs AccountController object.
     *
     * @param accountService account service instance
     */
    public AccountController(AccountService accountService) {
        this.gson = new Gson();
        this.accountService = accountService;
    }

    public void initRoutes() {
        Spark.get("/api/info", (req, resp) -> {
            resp.type(CONTENT_TYPE_APPLICATION_JSON);

            return "{" +
                    "\"application:\": \"Money transfer\"," +
                    "\"version\": 1.0" +
                    "}";
        });

        Spark.get("/api/account/:id", (req, resp) -> {
            String accountId = req.params("id");

            Account account = accountService.getById(accountId);
            return gson.toJson(account);
        });

        Spark.post("/api/account", (req, resp) -> {
            String body = req.body();

            Account newAccount = gson.fromJson(body, Account.class);

            accountService.save(newAccount);

            resp.status(200);
            return "{" +
                    "\"status\": \"account created\"" +
                    "}";
        });

        Spark.post("/api/transfer", (req, resp) -> {
            String body = req.body();

            TransferModel transferModel = gson.fromJson(body, TransferModel.class);

            BigDecimal amount = new BigDecimal(transferModel.getAmount());

            accountService.transfer(transferModel.getSrcAcc(), transferModel.getDestAcc(), amount);

            return "{" +
                    "\"status\": \"money successfully transferred\"" +
                    "}";
        });
    }

}
