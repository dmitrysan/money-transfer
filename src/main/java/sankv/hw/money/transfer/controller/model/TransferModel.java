package sankv.hw.money.transfer.controller.model;

/**
 * Model thar represents money transfer operation.
 */
public class TransferModel {

    /**
     * Source account.
     */
    private String srcAcc;

    /**
     * Destination account.
     */
    private String destAcc;

    /**
     * Amount of money to transfer.
     */
    private String amount;

    public String getSrcAcc() {
        return srcAcc;
    }

    public void setSrcAcc(String srcAcc) {
        this.srcAcc = srcAcc;
    }

    public String getDestAcc() {
        return destAcc;
    }

    public void setDestAcc(String destAcc) {
        this.destAcc = destAcc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
