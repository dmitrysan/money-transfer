package sankv.hw.money.transfer.repository.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountTest {

    private Account account;

    @Before
    public void setUp() throws Exception {
        account = new Account("1", new BigDecimal(100));
    }

    @Test
    public void should_replenish_balance() {
        account.replenish(new BigDecimal(100));

        Assert.assertEquals(new BigDecimal(200), account.getBalance());
    }

    @Test
    public void should_withdraw_from_balance() {
        account.withdraw(new BigDecimal(100));

        Assert.assertEquals(new BigDecimal(0), account.getBalance());
    }

    @Test
    public void should_transfer_money() {
        Account destAccount = new Account("2", new BigDecimal(0));

        account.transfer(destAccount, new BigDecimal(50));

        Assert.assertEquals(new BigDecimal(50), account.getBalance());
        Assert.assertEquals(new BigDecimal(50), destAccount.getBalance());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_to_transfer_money() {
        Account destAccount = new Account("2", new BigDecimal(0));

        account.transfer(destAccount, new BigDecimal(1000));

        Assert.assertEquals(new BigDecimal(50), account.getBalance());
        Assert.assertEquals(new BigDecimal(50), destAccount.getBalance());
    }
}