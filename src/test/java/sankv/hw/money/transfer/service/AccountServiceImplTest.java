package sankv.hw.money.transfer.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import sankv.hw.money.transfer.repository.AccountRepository;
import sankv.hw.money.transfer.repository.model.Account;

import java.math.BigDecimal;

public class AccountServiceImplTest {

    private AccountRepository accountRepository;

    private AccountService accountService;

    @Before
    public void setUp() throws Exception {
        accountRepository = Mockito.mock(AccountRepository.class);

        accountService = new AccountServiceImpl(accountRepository);
    }

    @Test
    public void should_save_account() {
        Account account = new Account("1", new BigDecimal(100));

        accountRepository.save(account);

        Mockito.verify(accountRepository, Mockito.times(1)).save(account);
    }

    @Test
    public void should_transfer_money_concurrently() throws InterruptedException {
        Account acc1 = new Account("acc1", new BigDecimal(10000));
        Account acc2 = new Account("acc2", new BigDecimal(1000));

        Mockito.when(accountRepository.getById("acc1")).thenReturn(acc1);
        Mockito.when(accountRepository.getById("acc2")).thenReturn(acc2);

        Runnable task1 = () -> {
            //transfered 1000 to acc2
            for (int i = 0; i < 1000; i++) {
                accountService.transfer(acc1.getId(), acc2.getId(), new BigDecimal(1));
            }
        };

        Runnable task2 = () -> {
            //transfered 100 to acc1
            for (int i = 0; i < 1000; i++) {
                accountService.transfer(acc2.getId(), acc1.getId(), new BigDecimal(1));
            }
        };

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        Assert.assertEquals(new BigDecimal(10000), acc1.getBalance());
        Assert.assertEquals(new BigDecimal(1000), acc2.getBalance());
    }

}