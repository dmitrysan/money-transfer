# Money transfer application

#### To build the project run the following command:

* mvn clean package 

##### To run the application use the following command:

* java -jar target/money-transfer-1.0.jar

#### Api:

* GET /api/info - Get application information

* GET /api/account/:id - Get an account info

            :id - account id

* POST /api/account - Save an account info

        {
            "id": "acc1",
            "balance": 100
        }

* POST /api/transfer - Transfer money between accounts

        {
            "srcAcc": "acc1",
            "destAcc": "acc2",
            "amount": 50
        }